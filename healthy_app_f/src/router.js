import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue'
//Añadimos los componentes
import Login from './components/Login.vue'
import SingUp from './components/SingUp.vue'
import Home from './components/Home.vue'
import Enfermeros from './components/Enfermeros.vue'
import Enfermero from './components/Enfermero.vue'
import Contrato from './components/Contrato.vue'
const routes = [
  {
    path: '/',
    name: 'root',
    component: App
  }, 
  {
    path: '/user/login',
    name:'logIn',
    component: Login
  },
  {
    path:'/user/signup',
    name:'signUp',
    component: SingUp
  },
  {
    path: '/user/home',
    name: 'home',
    component: Home
  },
  {
    path: '/enfermeros',
    name: 'enfermeros',
    component: Enfermeros
  },
  {
    path:'/:nombre/:id/',
    name:'enfermero',
    component:Enfermero
  },
  {
    path:'/contrato/:id_enfermero/',
    name:'contrato',
    component:Contrato
  }
 
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
